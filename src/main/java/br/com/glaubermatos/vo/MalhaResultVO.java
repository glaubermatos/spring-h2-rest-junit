package br.com.glaubermatos.vo;

import java.math.BigDecimal;

public class MalhaResultVO {
	
	private String rota;
	private BigDecimal custo;
	
	public MalhaResultVO() {}
	
	public MalhaResultVO(String rota, BigDecimal custo) {
		this.rota = rota;
		this.custo = custo;
	}
	
	
	public String getRota() {
		return rota;
	}
	public void setRota(String rota) {
		this.rota = rota;
	}
	public BigDecimal getCusto() {
		return custo;
	}
	public void setCusto(BigDecimal custo) {
		this.custo = custo;
	}
	
	

}
