package br.com.glaubermatos.vo;

import java.math.BigDecimal;


public class MalhaInputVO {

	private String origem;	
	private String destino;
	private BigDecimal autonomia;
	private BigDecimal valorLitro;
	
	public String getOrigem() {
		return origem;
	}
	public void setOrigem(String origem) {
		this.origem = origem;
	}
	public String getDestino() {
		return destino;
	}
	public void setDestino(String destino) {
		this.destino = destino;
	}
	public BigDecimal getAutonomia() {
		return autonomia;
	}
	public void setAutonomia(BigDecimal autonomia) {
		this.autonomia = autonomia;
	}
	public BigDecimal getValorLitro() {
		return valorLitro;
	}
	public void setValorLitro(BigDecimal valorLitro) {
		this.valorLitro = valorLitro;
	}
	
}
