package br.com.glaubermatos.ws.restful;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.glaubermatos.model.Malha;
import br.com.glaubermatos.service.MalhaService;
import br.com.glaubermatos.vo.MalhaInputVO;
import br.com.glaubermatos.vo.MalhaResultVO;


@Path("/malha")
public class MalhaWS {

	@Autowired
	private MalhaService malhaService;
	
	@GET 
	@Path("/list") @Produces(MediaType.APPLICATION_JSON)
	public List<Malha> obterMalhas() {
		return malhaService.obterTodas();
	}
	
	@POST
	@Path("/processar") @Produces(MediaType.APPLICATION_JSON)
	public MalhaResultVO processarMalhas(MalhaInputVO input) {
		MalhaResultVO result =  malhaService.encontrarMalhaMenorCusto(input);
		return result;
	}
	
	@POST
	@Path("/create") 
	public void cadastrarMalhas(List<Malha> malhas) {
		malhaService.salvar(malhas);
	}


}
