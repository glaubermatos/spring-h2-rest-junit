package br.com.glaubermatos.ws.restful;

import org.glassfish.jersey.server.ResourceConfig;

public class RestConfig extends ResourceConfig {

	public RestConfig() {
		register(MalhaWS.class);
	}

}