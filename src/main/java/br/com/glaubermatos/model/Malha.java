package br.com.glaubermatos.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="malha")
public class Malha {
	
	public Malha(){}
	
	public Malha(Integer idMalha) {
		this.idMalha = idMalha;
	}
	
	public Malha(Integer idMalha, String origem, String destino, Integer km,
			String mapa) {
		this.idMalha = idMalha;
		this.origem = origem;
		this.destino = destino;
		this.km = km;
		this.mapa = mapa;
	}



	@Id @Column(name="id_malha")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idMalha;
	
	@Column (length=120)
	private String origem;
	
	@Column (length=120)
	private String destino;
	
	@Column
	private Integer km;
	
	@Column (length=120)
	private String mapa;
	

	public Integer getIdMalha() {
		return idMalha;
	}
	public void setIdMalha(Integer idMalha) {
		this.idMalha = idMalha;
	}
	public String getOrigem() {
		return origem;
	}
	public void setOrigem(String origem) {
		this.origem = origem;
	}
	public String getDestino() {
		return destino;
	}
	public void setDestino(String destino) {
		this.destino = destino;
	}
	public Integer getKm() {
		return km;
	}
	public void setKm(Integer km) {
		this.km = km;
	}
	public String getMapa() {
		return mapa;
	}
	public void setMapa(String mapa) {
		this.mapa = mapa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idMalha == null) ? 0 : idMalha.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Malha other = (Malha) obj;
		if (idMalha == null) {
			if (other.idMalha != null)
				return false;
		} else if (!idMalha.equals(other.idMalha))
			return false;
		return true;
	}
	
	

}
