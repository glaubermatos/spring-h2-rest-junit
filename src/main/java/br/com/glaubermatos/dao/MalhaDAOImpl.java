package br.com.glaubermatos.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.glaubermatos.model.Malha;

@Repository
public class MalhaDAOImpl implements MalhaDAO {
	
	@PersistenceContext
	private EntityManager em;
	
	public Malha obterMalhaPelaOrigemDestino(String origem, String destino) {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT m ");
        sql.append(" FROM Malha m ");
        sql.append(" WHERE 1 = 1 ");
        if (origem != null)
        	sql.append(" AND m.origem = :origem ");
        if (destino != null)
        	sql.append(" AND m.destino = :destino ");
        sql.append(" ORDER BY m.km ");
		
		TypedQuery<Malha> query = em.createQuery(sql.toString(), Malha.class);
		
		
		if (origem != null)
			query.setParameter("origem", origem);
        if (destino != null)
        	query.setParameter("destino", destino);
        
        query.setMaxResults(1);
        Malha malha;
        try {
            malha = query.getSingleResult();			
		} catch (NoResultException e) {
			return null;
		}
        return malha;  	
    }
	
	public Malha obterMalhaPelaOrigem(String origem) {
		return this.obterMalhaPelaOrigemDestino(origem, null);
	}
	
	public Malha obterMalhaPeloDestino(String destino) {
		return this.obterMalhaPelaOrigemDestino(null, destino);
	}
	
	public List<Malha> obterTodas() {
		TypedQuery<Malha> query = em.createQuery("SELECT m FROM Malha m ORDER BY m.km", Malha.class);
		return query.getResultList();
	}

	@Transactional
	public void salvar(List<Malha> listMalhas) {
		for (Malha malha : listMalhas)
			em.persist(malha);
	}
	
	public Malha obterPeloId(Integer id) {
		return em.find(Malha.class, id);
	}
	

}
