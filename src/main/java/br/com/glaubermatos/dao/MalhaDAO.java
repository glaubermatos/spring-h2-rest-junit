package br.com.glaubermatos.dao;

import java.util.List;

import br.com.glaubermatos.model.Malha;

public interface MalhaDAO {
	
	public Malha obterMalhaPelaOrigemDestino(String origem, String destino);
	public Malha obterMalhaPelaOrigem(String origem);
	public Malha obterMalhaPeloDestino(String destino);
	public List<Malha> obterTodas();
	public void salvar(List<Malha> listMalhas);
	public Malha obterPeloId(Integer id);
	
}
