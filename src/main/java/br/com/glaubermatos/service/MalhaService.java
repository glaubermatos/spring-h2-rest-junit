package br.com.glaubermatos.service;

import java.util.List;

import br.com.glaubermatos.model.Malha;
import br.com.glaubermatos.vo.MalhaInputVO;
import br.com.glaubermatos.vo.MalhaResultVO;

public interface MalhaService {

	public Malha obterMalhaPelaOrigemDestino(String origem, String destino);
	public Malha obterMalhaPelaOrigem(String origem);
	public Malha obterMalhaPeloDestino(String destino);
	public List<Malha> obterTodas();
	public MalhaResultVO encontrarMalhaMenorCusto(MalhaInputVO input);
	public void salvar(List<Malha> listMalhas);
	public Malha obterPeloId(Integer id);
}
