package br.com.glaubermatos.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.glaubermatos.dao.MalhaDAO;
import br.com.glaubermatos.model.Malha;
import br.com.glaubermatos.vo.MalhaInputVO;
import br.com.glaubermatos.vo.MalhaResultVO;

@Component
public class MalhaServiceImpl implements MalhaService {
	
	@Autowired
	MalhaDAO malhaDAO;
	
	public MalhaServiceImpl() {}
	
	public MalhaServiceImpl(MalhaDAO malhaDAO) {
		this.malhaDAO = malhaDAO;
	}
	
	public Malha obterMalhaPelaOrigemDestino(String origem, String destino) {
		return malhaDAO.obterMalhaPelaOrigemDestino(origem, destino);
	}
	
	public Malha obterMalhaPelaOrigem(String origem) {
		return malhaDAO.obterMalhaPelaOrigem(origem);
	}
	
	public Malha obterMalhaPeloDestino(String destino) {
		return malhaDAO.obterMalhaPeloDestino(destino);
	}
	
	public List<Malha> obterTodas() {
		return malhaDAO.obterTodas();
	}
	
	public void salvar(List<Malha> listMalhas) {
		malhaDAO.salvar(listMalhas);
	}
	
	public Malha obterPeloId(Integer id) {
		return malhaDAO.obterPeloId(id);
	}
	
	public MalhaResultVO encontrarMalhaMenorCusto(MalhaInputVO input) {
		
		List<Malha> listMalhas = new ArrayList<Malha>();
		
		Malha malha = this.obterMalhaPelaOrigemDestino(input.getOrigem(), input.getDestino());
		
		if (malha != null) {//Se encontrou de primeira, retorna a rota
			return this.montarResposta(malha, input);
		} else {//Do contrario continua pesquisando 
			
			Malha malhaOrigem = this.obterMalhaPelaOrigem(input.getOrigem());
			if (malhaOrigem == null)
				return null;
			
			listMalhas.add(malhaOrigem);
			
			Malha malhaDestino = this.obterMalhaPelaOrigem(malhaOrigem.getDestino());
			if (malhaDestino == null)
				return null;
			
			listMalhas.add(malhaDestino);

			if (malhaDestino.getDestino().equals(input.getDestino())) {//se encontrou na segunda tentativa retorna o resultado
				return this.montarResposta(listMalhas, input);
			} else {//do contrário continua pesquisando
				Malha malhaTemp = malhaDestino;
				while (true) {
					malhaTemp = this.obterMalhaPelaOrigem(malhaTemp.getDestino());
					if (malhaTemp == null)
						return null;
					listMalhas.add(malhaTemp);
					if (malhaTemp.getDestino().equals(input.getDestino())) 
						break;					
				}
				return this.montarResposta(listMalhas, input);
			}
		}
	}
	
	private MalhaResultVO montarResposta(Malha malha, MalhaInputVO input) {
		
		String rota = malha.getOrigem() + " " +malha.getDestino();
		BigDecimal custo = this.calculaCusto(malha, input);
		MalhaResultVO result = new MalhaResultVO(rota, custo);
		
		return result;
	}
	
	private MalhaResultVO montarResposta(List<Malha> listMalhas, MalhaInputVO input) {
		
		String rota = new String();
		BigDecimal custo = new BigDecimal(0);
		for (Malha malha : listMalhas) {
			rota += malha.getOrigem() + " " +malha.getDestino()+" ";
			custo = custo.add(this.calculaCusto(malha, input));
		}
		MalhaResultVO result = new MalhaResultVO(rota, custo);
		return result;
	}
	
	private BigDecimal calculaCusto(Malha malha, MalhaInputVO input) {
		
		BigDecimal custo = new BigDecimal(malha.getKm());
		custo = custo.divide(input.getAutonomia(), 2, RoundingMode.CEILING);
		custo = custo.multiply(input.getValorLitro());
		
		return custo;
	}

}
