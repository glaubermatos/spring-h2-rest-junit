package br.com.glaubermatos;

import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Arrays;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import br.com.glaubermatos.dao.MalhaDAO;
import br.com.glaubermatos.model.Malha;
import br.com.glaubermatos.service.MalhaServiceImpl;
import br.com.glaubermatos.vo.MalhaInputVO;
import br.com.glaubermatos.vo.MalhaResultVO;

@RunWith(MockitoJUnitRunner.class)
public class MalhaTC {

	@Mock
	private MalhaDAO malhaDAO;

    public static Client client = null;

    @BeforeClass
    public static void Before() {
    	MalhaTC.client = ClientBuilder.newClient();
    }
	
	@Before
	public void setup() {

	}

	@Test
	public void test() throws Exception {
		
		WebTarget target = client.target("http://localhost:8080/ciandt-teste/ws/malha/processar");
		
		MalhaInputVO inputVO = new MalhaInputVO();
		inputVO.setAutonomia(new BigDecimal(10));
		inputVO.setOrigem("A");
		inputVO.setDestino("D");
		inputVO.setValorLitro(new BigDecimal(2.5));
		MalhaResultVO result = target.request(MediaType.APPLICATION_JSON).post(
				Entity.entity(inputVO, MediaType.APPLICATION_JSON),
				MalhaResultVO.class);
		
	
		Assert.assertTrue(result.getCusto().compareTo(new BigDecimal(6.25)) == 0);
		
		when(malhaDAO.obterMalhaPelaOrigemDestino("", "")).thenReturn(new Malha());
		when(malhaDAO.obterTodas()).thenReturn(Arrays.asList(new Malha(1), new Malha(2)));

		Assert.assertThat(new MalhaServiceImpl(malhaDAO).obterTodas(), Matchers.not(Matchers.empty()));
		Assert.assertThat(new MalhaServiceImpl(malhaDAO).obterTodas(), Matchers.contains(new Malha(1), new Malha(2)));
		
	}

}
