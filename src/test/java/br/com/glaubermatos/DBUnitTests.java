package br.com.glaubermatos;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import br.com.glaubermatos.model.Malha;
import br.com.glaubermatos.service.MalhaService;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class })	
public class DBUnitTests {
	
	@Autowired
	private MalhaService malhaService;
	
	@Test @DatabaseSetup("data-load.xml")
	public void testFind() throws Exception {
		
		Malha malha = malhaService.obterPeloId(1);
		Assert.assertEquals(malha.getIdMalha(), new Integer(1));
		
		malha = malhaService.obterMalhaPelaOrigemDestino("A", "B");
		Assert.assertEquals(malha.getKm(), new Integer(10));
		
		List<Malha> listMalhas = new ArrayList<Malha>();
		
		listMalhas.add(new Malha(null, "C", "D", 30, "mapa-4"));
		listMalhas.add(new Malha(null, "B", "E", 50, "mapa-5"));
		listMalhas.add(new Malha(null, "C", "D", 30, "mapa-6"));
		
		malhaService.salvar(listMalhas);
		
		listMalhas = malhaService.obterTodas();
		
		Assert.assertThat(listMalhas, Matchers.hasSize(6));
		Assert.assertThat(listMalhas, Matchers.hasItem(new Malha(5)));
	}

}
