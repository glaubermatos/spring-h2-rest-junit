# Sobre o Projeto

O projeto consiste em um serviço REST com suas respectivas camadas  ws, service, model e DAO. 
O pacote principal é br.com.glaubermatos (dentro de src/main/java) e o pacote de teste é o mesmo porém em src/test/java.

# Ambiente

* Tomcat 7.0.54
* H2 embedded in-memory 1.4.178
* Jersey 2.8
* Spring 4.0.4
* JUnit, JMockit, Hamcrest
# Executando o Aplicativo

- Como é um projeto maven basta importar o mesmo no eclipse como "maven project" ou clonar o mesmo e usar mvn clean install
- Uma vez importado basta adicionar o servidor e executar o app, nos meus testes utilizei o Tomcat 7.0.54
- Existe um arquivo index.html com alguns detalhes adicionais de funcionamento, além de links para algumas páginas utilitárias para uso do serviço.

# Consumindo o REST

- Execute o aplicativo e acesse a página (http://localhost:8080/ciandt-teste/index.html) e veja mais detalhes com exemplos de operação

# Executando os testes

No eclipse vá para src/test/java e no topo do package br.com.glaubermatos, clique com o botão direito e selecione Run as => JUnit Test.
	
# O que ficou faltando

Ficou faltando utilizar o JerseyTest e o DBUnit para deixar os testes mais completos. Porém não deu tempo.